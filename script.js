
(async function() {
console.log('SCRIPT START');
const PIIsdk = (await import('@notabene/pii-sdk')).default;
const {initAgent} = await import('@notabene/pii-sdk');
const piiSdk = new PIIsdk({
  kmsSecretKey: '75099860d284bb22a2c96a6e41ee024d04171a4ba33b2f3720d2bec17d1ced78',
  piiURL: 'https://pii.notabene.dev',
  clientId: '7iQ6MNgumyOywH6lyWdYcEk8ACHqT19Z',
  clientSecret: '1rg17YZtmFTcml95UHRRl1g1dfMbhhLUhQnqTUIWSaH7y3N62tUMRxQ7HtWsAWfz',
  audience: 'https://pii.notabene.dev',
  authURL: 'https://auth.notabene.id'
});

console.log('SCRIPT piiSdk ===============================>', piiSdk);

const piiData = {
  originator: {
    originatorPersons: [
      {
        naturalPerson: {
          name: [
            {
              nameIdentifier: [
                {
                  primaryIdentifier: 'Frodo',
                  secondaryIdentifier: 'Baggins',
                  nameIdentifierType: 'LEGL'
                }
              ]
            }
          ],
          nationalIdentification: {
            nationalIdentifier: 'AABBCCDDEEFF0011223344',
            nationalIdentifierType: 'CCPT',
            countryOfIssue: 'NZ'
          },
          dateAndPlaceOfBirth: {
            dateOfBirth: '1900-01-01',
            placeOfBirth: 'Planet Earth'
          },
          geographicAddress: [
            {
              addressLine: ['Cool Road /-.st'],
              country: 'BE',
              addressType: 'HOME'
            }
          ]
        }
      }
    ],
    accountNumber: ['01234567890']
  },
  beneficiary: {
    beneficiaryPersons: [
      {
        naturalPerson: {
          name: [
            {
              nameIdentifier: [
                {
                  primaryIdentifier: 'Bilbo',
                  secondaryIdentifier: 'Bolson',
                  nameIdentifierType: 'LEGL'
                }
              ]
            }
          ]
        }
      }
    ],
    accountNumber: ['01234567890']
  }
};

console.log('SCRIPT piiSdk afeter ===============================>',);

    // Your code to generate encrypted PII
    const kmsSecretKey = '75099860d284bb22a2c96a6e41ee024d04171a4ba33b2f3720d2bec17d1ced78';
    const jsonDidKey = '{\"did\":\"did:key:z6MknL8ERKo2MqArMvJdA2EdZcUWehR7t3gDDc9hsbB7TCfZ\",\"controllerKeyId\":\"75099860d284bb22a2c96a6e41ee024d04171a4ba33b2f3720d2bec17d1ced78\",\"keys\":[{\"type\":\"Ed25519\",\"kid\":\"75099860d284bb22a2c96a6e41ee024d04171a4ba33b2f3720d2bec17d1ced78\",\"publicKeyHex\":\"75099860d284bb22a2c96a6e41ee024d04171a4ba33b2f3720d2bec17d1ced78\",\"meta\":{\"algorithms\":[\"Ed25519\",\"EdDSA\"]},\"kms\":\"local\",\"privateKeyHex\":\"c0add0d8b45f704bbc8235d05ee449dc19453dce94df2b07c7bddb36cf7de59475099860d284bb22a2c96a6e41ee024d04171a4ba33b2f3720d2bec17d1ced78\"}],\"services\":[],\"provider\":\"did:key\"}'
            agent = initAgent({ KMS_SECRET_KEY: kmsSecretKey }).agent;
            console.log("AGENT  ________________________", agent);
            await agent.didManagerImport(JSON.parse(jsonDidKey));
            const counterpartyDIDKey = undefined;
            
            console.log("AGENT  ________________________", piiData);
            
            piiIvms = await piiSdk.generatePIIField({
                piiData,
                originatorVASPdid: 'did:ethr:0x44957e75d6ce4a5bf37aae117da86422c848f7c2',
                beneficiaryVASPdid: 'did:ethr:0xf9139d9ca3cd9824a7fb623b1d34618a155137bc',
                counterpartyDIDKey,
                agent,
                senderDIDKey: JSON.parse(jsonDidKey).did,
                encryptionMethod: 2,
            }).catch((err) => {
                console.log("ERROR ======================>", err);
            });
    console.log('Encrypted PII:', piiIvms);

    // Get the absolute path to the output file
    // const outputPath = path.resolve('output.txt');

    // Create the directory if it doesn't exist
    // const outputDir = path.dirname(outputPath);
    // if (!fs.existsSync(outputDir)) {
    //   fs.mkdirSync(outputDir, { recursive: true });
    // }

    // Write the encryptedPII to the output file
    // fs.writeFileSync(outputPath, encryptedPII);

    // Print a success message
    console.log('Encryption completed successfully');

    // console.error(err);
    console.log('SCRIPT END');
})();
