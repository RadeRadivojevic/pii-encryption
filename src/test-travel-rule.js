// import { PIIEncryption } from "./pii-encryption.js";
//
// const config = {
//     kmsSecretKey:
//         "75099860d284bb22a2c96a6e41ee024d04171a4ba33b2f3720d2bec17d1ced78",
//     authURL: "https://auth.notabene.id",
//     baseURL: "https://api.notabene.dev",
//     audience: "https://api.notabene.dev",
//     baseURLPII: "https://pii.notabene.dev",
//     audiencePII: "https://pii.notabene.dev",
//     clientId: "7iQ6MNgumyOywH6lyWdYcEk8ACHqT19Z",
//     clientSecret: "1rg17YZtmFTcml95UHRRl1g1dfMbhhLUhQnqTUIWSaH7y3N62tUMRxQ7HtWsAWfz",
// }
//
// const message = {
//     transactionAsset: "ETH",
//     transactionAmount: "1111111000000000000",
//     originatorVASPdid: "did:ethr:0x44957e75d6ce4a5bf37aae117da86422c848f7c2",
//     beneficiaryVASPdid: "did:ethr:0xafa31a469bddca4323cde5c1f98d4fd5ec805739",
//     originator: {
//         originatorPersons: [
//             {
//                 naturalPerson: {
//                     name: [
//                         {
//                             nameIdentifier: [
//                                 {
//                                     primaryIdentifier: "Frodo",
//                                     secondaryIdentifier: "Baggins",
//                                     nameIdentifierType: "LEGL",
//                                 },
//                             ],
//                         },
//                     ],
//                     nationalIdentification: {
//                         nationalIdentifier: "AABBCCDDEEFF0011223344",
//                         nationalIdentifierType: "CCPT",
//                         countryOfIssue: "NZ",
//                     },
//                     dateAndPlaceOfBirth: {
//                         dateOfBirth: "1900-01-01",
//                         placeOfBirth: "Planet Earth",
//                     },
//                     geographicAddress: [
//                         {
//                             addressLine: ["Cool Road /-.st"],
//                             country: "BE",
//                             addressType: "HOME",
//                         },
//                     ],
//                 },
//             },
//         ],
//         accountNumber: ["01234567890"],
//     },
//     beneficiary: {
//         beneficiaryPersons: [
//             {
//                 naturalPerson: {
//                     name: [
//                         {
//                             nameIdentifier: [
//                                 {
//                                     primaryIdentifier: "Bilbo",
//                                     secondaryIdentifier: "Bolson",
//                                     nameIdentifierType: "LEGL",
//                                 },
//                             ],
//                         },
//                     ],
//                 },
//             },
//         ],
//         accountNumber: ["01234567890"],
//     },
// };
//
// const { PIIsdk, PIIEncryptionMethod } = require('@notabene/pii-sdk');
// const piiSdk = new PIIsdk({
//     kmsSecretKey: '${kmsSecretKey}',
//     piiURL: '${this.config.baseURLPII}',
//     clientId: '${this.config.clientId}',
//     clientSecret: '${this.config.clientSecret}',
//     audience: '${this.config.audiencePII}',
//     authURL: '${this.config.authURL}',
// });
// const piiData = '${JSON.stringify(pii)}';
// async function encryptPIIData() {
//     try {
//         const encryptionParams = {
//             pii: piiData,
//             originatorVASPdid: '${travelRuleMessage.originatorVASPdid}',
//             beneficiaryVASPdid: '${travelRuleMessage.beneficiaryVASPdid}',
//             counterpartyDIDKey: '${counterpartyDIDKey}',
//             agent: null,
//             senderDIDKey: '${JSON.stringify(JSON.parse(jsonDidKey).did)}',
//             encryptionMethod: PIIEncryptionMethod.HYBRID,
//         };
//         const encryptedPII = await piiSdk.generatePIIField(encryptionParams);
//         console.log('Encrypted PII:', encryptedPII);
//         travelRuleMessage.beneficiary = encryptedPII.beneficiary;
//         travelRuleMessage.originator = encryptedPII.originator;
//         travelRuleMessage.beneficiary.originatorPersons = encryptedPII.beneficiary.beneficairyPersons;
//         travelRuleMessage.beneficiary.accountNumber = encryptedPII.beneficiary.accountNumber;
//     } catch (error) {
//         console.error('Encryption failed:', error);
//     }
// }
// encryptPIIData();
//
// function encryptTravelRuleMessage(message) {
//     const piiEncryption = new PIIEncryption(config);
//     return piiEncryption.hybridEncode(message);
// }
//
// encryptTravelRuleMessage(message).then((piiMessage) => {
//     console.log(piiMessage);
// }).catch((error) => {
//     console.log(error);
// });