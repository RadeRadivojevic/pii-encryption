// import { PII } from "travel-rule-pii";
import PIIsdk, { PIIEncryptionMethod } from "@notabene/pii-sdk";
import { TransactionArguments, TravelRule, TravelRuleEncryptionOptions, TravelRuleOptions } from "./types";
import * as util from "util";

require("dotenv").config();

type OwnershipProof = {
    type: string;
    proof: string;
};


// create TR TX
const travelRulePlain = {"transactionAsset": "BTC", "transactionAmount": "10000", "transactionBlockchainInfo": {"txHash": "", "origin": "5342b5234hioutewry87y78sdfghy783t4t34", "destination": "5643jn5h34y2g7hg42jt24j890y345gfgh65"}, "originatorVASPdid": "did:ethr:0x5dddec8dd669c49304ddbcba36ccc6e0798534dc", "travelRuleBehavior": false, "beneficiaryVASPdid": "did:ethr:0x4625327acb8cfab59387c03ef87c7ccbf23794d4", "beneficiaryVASPname": "", "originator": {"originatorPersons": [{"naturalPerson": {"name": [{"nameIdentifier": [{"primaryIdentifier": "Vlada", "secondaryIdentifier": "Markovic"}]}], "geographicAddress": [{"streetName": "streetName", "buildingNumber": "buildingNumber", "buildingName": "buildingName", "townName": "townName", "countrySubDivision": "countrySubDivision", "postCode": "postCode"}], "nationalIdentification": {"nationalIdentifier": "nationalIdentifier", "nationalIdentifierType": "DRLC", "registrationAuthority": "registrationAuthority"}, "dateAndPlaceOfBirth": {"dateOfBirth": "1999-01-01", "placeOfBirth": "SR"}}}], "accountNumber": ["accountNumber"]}, "beneficiary": {"beneficiaryPersons": [{"naturalPerson": {"name": [{"nameIdentifier": [{"primaryIdentifier": "Pavle", "secondaryIdentifier": "Krivokuca"}]}], "geographicAddress": [{"streetName": "streetName", "buildingNumber": "buildingNumber", "buildingName": "buildingName", "townName": "townName", "countrySubDivision": "countrySubDivision", "postCode": "postCode"}]}}], "accountNumber": ["accountNumber"]}};


const BCTransaction = {
    assetId: "ETH",
    source: {
        type: "VAULT_ACCOUNT",
        id: "0",
        virtualId: undefined,
        virtualType: undefined
    },
    destination: {
        type: "ONE_TIME_ADDRESS",
        id: undefined,
        oneTimeAddress: {
            address: "0xA0C27827A8454ae6e2F9123442e37A90B073A76b",
            tag: undefined
        },
        virtualId: undefined,
        virtualType: undefined
    },
    operation: "TRANSFER",
    amount: "0.2",
    fee: undefined,
    gasPrice: undefined,
    gasLimit: undefined,
    feeLevel: undefined,
    maxFee: undefined,
    failOnLowFee: false,
    priorityFee: undefined,
    note: "Created with love by fireblocks SDK from BDD Travel Rule TEST",
    autoStaking: undefined,
    cpuStaking: undefined,
    networkStaking: undefined,
    replaceTxByHash: "",
    extraParameters: undefined,
    destinations: undefined,
    externalTxId: undefined,
    treatAsGrossAmount: undefined,
    travelRuleMessage: undefined,
};
BCTransaction.travelRuleMessage = travelRulePlain;

const requiredFields = [
    "baseURLPII",
    "audiencePII",
    "clientId",
    "clientSecret",
    "authURL",
    "jsonDidKey",
];

export class PIIEncryption {
    public toolset: PIIsdk;

    constructor(private readonly config: TravelRuleOptions) {
        this.config = config;
        const missingFields = requiredFields.filter(
            (field) => !(field in this.config)
        );

        if (missingFields.length > 0) {
            throw new Error(
                `Missing PII configuration fields: ${missingFields.join(", ")}`
            );
        }

        this.toolset = new PIIsdk({
            piiURL: config.baseURLPII,
            audience: config.audiencePII,
            clientId: config.clientId,
            clientSecret: config.clientSecret,
            authURL: config.authURL,
        });
    }

    async hybridEncode(transaction: any, TravelRuleEncryptionOptions?: TravelRuleEncryptionOptions) {
        const { travelRuleMessage } = transaction;
        const pii = travelRuleMessage.pii || {
            originator: travelRuleMessage.originator,
            beneficiary: travelRuleMessage.beneficiary,
        };
        const { jsonDidKey } = this.config;
        const counterpartyDIDKey = TravelRuleEncryptionOptions?.beneficiaryPIIDidKey || undefined;

        let piiIvms;

        try {
            piiIvms = await this.toolset.generatePIIField({
                pii,
                originatorVASPdid: travelRuleMessage.originatorVASPdid,
                beneficiaryVASPdid: travelRuleMessage.beneficiaryVASPdid,
                counterpartyDIDKey,
                keypair: JSON.parse(jsonDidKey),
                senderDIDKey: JSON.parse(jsonDidKey).did,
                encryptionMethod: TravelRuleEncryptionOptions?.sendToProvider
                    ? PIIEncryptionMethod.HYBRID
                    : PIIEncryptionMethod.END_2_END,
            });
        } catch (error) {
            const errorMessage = error.message || error.toString();
            const errorDetails = JSON.stringify(error);
            throw new Error(`Failed to generate PII fields error: ${errorMessage}. Details: ${errorDetails}`);
        }

        transaction.travelRuleMessage = this.travelRuleMessageHandler(travelRuleMessage, piiIvms);

        return transaction;
    }

    private travelRuleMessageHandler(travelRuleMessage: any, piiIvms: any): any {
        travelRuleMessage.beneficiary = piiIvms.beneficiary;
        travelRuleMessage.originator = piiIvms.originator;

        travelRuleMessage.beneficiary = {
            originatorPersons: piiIvms.beneficiary.beneficiaryPersons,
            accountNumber: piiIvms.beneficiary.accountNumber,
        };

        return travelRuleMessage;
    }
}

console.log("CLIENT ID ==============", process.env.NOTABENE_CLIENT_ID);

(async () => {
    const client = new PIIEncryption({
        baseURLPII: process.env.NOTABENE_BASE_URL_PII,
        audiencePII: process.env.NOTABENE_AUDIENCE_PII,
        clientId: process.env.NOTABENE_CLIENT_ID,
        clientSecret: process.env.NOTABENE_CLIENT_SECRET,
        authURL: process.env.NOTABENE_AUTH_URL,
        jsonDidKey: process.env.JSON_DID_KEY,
    });
    const transaction = await client.hybridEncode(BCTransaction);
    console.log("TRANSACTION", JSON.stringify(transaction, null, 2));
})()